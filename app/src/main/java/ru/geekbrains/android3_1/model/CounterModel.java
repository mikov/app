package ru.geekbrains.android3_1.model;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


public class CounterModel {
    private List<Integer> counters;

    public CounterModel() {
        counters = new ArrayList<>();
        counters.add(0);
        counters.add(0);
        counters.add(0);
    }

    public Observable<Integer> getAt(int index) {
        return Observable.fromCallable(() -> {
            counters.set(index, counters.get(index) + 1);
            return counters.get(index);
        });
    }

    public int setAt(int index, int value) {
        return counters.set(index, value);
    }
}
