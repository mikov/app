package ru.geekbrains.android3_1.view;

import android.os.Bundle;
import android.widget.Button;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.jakewharton.rxbinding2.view.RxView;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.disposables.Disposable;
import ru.geekbrains.android3_1.R;
import ru.geekbrains.android3_1.presenter.MainPresenter;

public class MainActivity extends MvpAppCompatActivity implements MainView {
    @InjectPresenter MainPresenter presenter;

    @BindView(R.id.btn_one) Button button1;
    @BindView(R.id.btn_two) Button button2;
    @BindView(R.id.btn_three) Button button3;


    protected Disposable button1Subscription;
    protected Disposable button2Subscription;
    protected Disposable button3Subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        onButton1Click();
        onButton2Click();
        onButton3Click();
    }

    private void onButton3Click() {
        button1Subscription = RxView.clicks(button1).subscribe(o -> presenter.onButtonOneClick());
    }

    private void onButton2Click() {
        button2Subscription = RxView.clicks(button2).subscribe(o -> presenter.onButtonTwoClick());
    }

    private void onButton1Click() {
        button3Subscription = RxView.clicks(button3).subscribe(o -> presenter.onButtonThreeClick());

    }

//    @ProvidePresenter
//    public MainPresenter createPresenter() {
//        return new MainPresenter();
//    }
//
//    @OnClick({R.id.btn_one})
//    public void buttonOneClick(Button button) { presenter.onButtonOneClick();
//    }
//
//    @OnClick({R.id.btn_two})
//    public void buttonTwoClick(Button button) {
//        presenter.onButtonTwoClick();
//    }
//
//    @OnClick({R.id.btn_three})
//    public void buttonThreeClick(Button button) {
//        presenter.onButtonThreeClick();
//    }


    @Override
    public void setOneValue(int value) {
        button1.setText(String.format(getString(R.string.count_format), value));
    }

    @Override
    public void setTwoValue(int value) {
        button2.setText(String.format(getString(R.string.count_format), value));
    }

    @Override
    public void setThreeValue(int value) {
        button3.setText(String.format(getString(R.string.count_format), value));
    }
}
