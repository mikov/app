package ru.geekbrains.android3_1.presenter;


import com.arellomobile.mvp.MvpPresenter;

import ru.geekbrains.android3_1.model.CounterModel;
import ru.geekbrains.android3_1.view.MainView;


public class MainPresenter extends MvpPresenter<MainView> {
    private CounterModel model;

    public MainPresenter() {
        this.model = new CounterModel();
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void onButtonOneClick() {
        model.getAt(0).subscribe(integer -> getViewState().setOneValue(integer));
    }

    public void onButtonTwoClick() {
        model.getAt(0).subscribe(integer -> getViewState().setOneValue(integer));
    }

    public void onButtonThreeClick() {
        model.getAt(0).subscribe(integer -> getViewState().setOneValue(integer));
    }
}
